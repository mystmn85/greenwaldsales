from flask import Flask, render_template, request, flash
from .config import Configurationdb

import random

''' DEFINE APP '''
app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
WTF_CSRF_SECRET_KEY = Configurationdb.SECRET_KEY

app.config.from_object(Configurationdb)

@app.errorhandler(404)
def not_found(e):
    return render_template("404.html")

# DEFINE Routes
@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/test', methods=['GET', 'POST'])
def test():
    return render_template('manufacturers.html')

@app.route('/popout', methods=['GET', 'POST'])
def popout():
    return render_template('popout.html')

@app.route('/hover-over', methods=['GET'])
def hoverOver():
    return render_template('hover-over.html')

@app.route('/tiny-slider', methods=['GET'])
def tinySlider():
    return render_template('tiny-slider.html')