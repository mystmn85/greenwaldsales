import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Configuration(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "sf$9@RpdwembBXoX8OEv0FIDYA^BWEjrXSc0WM&a"
    DEBUG = True
    CSRF_ENABLED = True

class Configurationdb(Configuration):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


'''
    DEFAULT_URI = "sqlite:///" + os.path.join(basedir, "app.db")
    SQLALCHEMY_DATABASE_URI = DEFAULT_URI
'''

SECRET_KEY = os.urandom(64)
